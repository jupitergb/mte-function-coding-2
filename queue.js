let collection = [];



function print() {    

    return collection;
}

function enqueue(element) {   

   collection[collection.length] = element;
   return collection;
}

function dequeue() {
    
    delete collection[0];

    for (i = 0; i < collection.length; i++) {
        collection[i] = collection[i+1];
    }
    collection.length -= 1;

    return collection;
}

function front() {
    
    return collection[0];
}

function size() {    

    return collection.length;
}

function isEmpty() {    

    return collection.length === 0 ? true : false;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};